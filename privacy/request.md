[Korean]

#### f-MES 수발주앱 계정 삭제 요청
- f-MES 수발주 앱 계정 삭제 요청 시, 아래 정보를 lsefmes@gmail.com으로 보내주세요.
- 이 때 반드시, 발신메일은 가입하실 때 사용한 이메일 주소이어야 합니다.
- 가입하실 때 사용한 이메일로 아래 정보를 함께 보내주세요.
 * 사업자등록번호: 
- 계정 삭제는 해당 발신 이메일 주소와 사업자등록번호가 매칭 될 시, 영구 삭제 됩니다.

[English]
#### Request for deletion of f-MES ordering app account
- When requesting deletion of the f-MES subcontractor app account, please send the information below to lsefmes@gmail.com.
- At this time, the sending email must be the email address you used when signing up.
- Please send the information below to the email address you used when signing up.
 * Company Registration Number:
- Account deletion will be permanently deleted when the sending email address matches the business registration number.
